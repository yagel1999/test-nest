import { createLogger, transports, format, Logger as WinstonLogger } from "winston";

export const logger: WinstonLogger = createLogger({
    transports: [new transports.Console()],
    format: format.combine(format.timestamp(), format.json()),
    exitOnError: false
});