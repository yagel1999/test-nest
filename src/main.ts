import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import { logger } from "./logger";

const port = 3000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { 
    logger: WinstonModule.createLogger({
      instance: logger,
      
    }),
    // logger: false
  });
  await app.listen(port).then(() => {
    logger.info(`Server is up on port ${port}`)
  });
}
bootstrap();
