import { Injectable, Logger } from '@nestjs/common';
import { logger } from "./logger";

@Injectable()
export class AppService {
  // private logger: Logger = new Logger(AppService.name);

  getHello(): string {
    logger.log('this is an info message', { halo: 'mi ze' });
    logger.error('this is an error message');

    return 'Hello World!';
  }
}
